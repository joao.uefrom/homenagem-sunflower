const container = document.getElementById('heart');
let ctx = null;
let counter = 0;
let executionLimit = 0;

function isInsideHeart(x, y) {
    const pixel = ctx.getImageData(x, y, 1, 1).data;

    return pixel[3] !== 0;
}

function getRandomPosition(maxX, maxY) {
    while (true) {
        const newX = Math.floor(Math.random() * maxX) + 1;
        const newY = Math.floor(Math.random() * maxY) + 1;

        if (!isInsideHeart(newX, newY)) continue;

        return [newX, newY];
    }
}

function createSunflower() {
    const sunflower = document.createElement('img');
    sunflower.src = 'images/sunflower.png';
    sunflower.classList.add('sunflower');

    return sunflower;
}

function addSunflower() {
    const heart = document.getElementById('heart');
    const sunflower = createSunflower();
    heart.appendChild(sunflower);

    const [randomX, randomY] = getRandomPosition(heart.clientWidth, heart.clientHeight);
    
    sunflower.style.left = `${randomX}px`
    sunflower.style.top = `${randomY}px`

    sunflower.onload = function() {
        sunflower.style.opacity = 1;
    }
}

function configCanvas() {
    const canvas = document.createElement('canvas');
    heart.appendChild(canvas);

    canvas.width = heart.offsetWidth;
    canvas.height = heart.offsetHeight;
    canvas.style.position = 'absolute';
    canvas.style.display = 'none';

    ctx = canvas.getContext('2d');
    const img = new Image();
    img.src = 'images/heart.png';

    img.onload = function () {
        const aspectRatio = img.width / img.height;
        const newWidth = canvas.width;
        const newHeight = canvas.width / aspectRatio;

        const x = 0;
        const y = (canvas.height - newHeight) / 2;

        ctx.drawImage(img, x, y, newWidth, newHeight);
    }

}

function init() {
    configCanvas();
    const days = document.getElementById('days');

    const diffTime = Math.abs(new Date('2023-10-16') - new Date('2020-02-24'));
    const diffDays = Math.ceil(diffTime / (24 * 60 * 60 * 1000));
    executionLimit = diffDays;

    const intervalId = setInterval(function () {
        addSunflower();
        counter++;

        days.innerText = 'por incriveis ' + counter + ' dias :3'

        if (counter == executionLimit) {
            clearInterval(intervalId);
        }
    }, 100);
}

window.onload = init;